# Corer - Moved to Github

Corer has been moved to [Github](https://github.com/gi-bielefeld/corer). Please refer to the Corer repository on Github in order to receive the tool's most recent version!
